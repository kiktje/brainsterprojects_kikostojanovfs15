# BrainsterProjects_KikoStojanovFS15

# Project-01 | Brainster Labs

## Kiko Stojanov | Full Stack Academy

Brainster Labs is a platform(project) where students from Brainster's marketing, design, and programming academies showcase the projects they have created.

## Design

|   DESIGN: https://prnt.sc/QuHVekNq0DF1   |

This page consists of several sections:

1. Top menu (navbar):
    - Logo
    - Marketing Academy that will lead to the following link: https://brainster.co/marketing/
    - Programming Academy that will lead to the following link: https://brainster.co/full-stack/
    - Data-Science Academy that will lead to the following link: https://brainster.co/data-science/
    - Design Academy that will lead to the following link: https://brainster.co/graphic-design/
    - "Hire a Student" Button which will lead to a new page with a form.
2. Banner with image and text.
3. Filters.
4. 20 Cards with projects from the academy.
5. Footer.

|   Cards   |
The index page initially shows a total of 20 cards, of which 10 will
be for Programming, 4 for Design and 6 for Marketing.
With mouseover on one of the card, the card should be increased in size.

|   Filters   |
Above the cards are 3 buttons/checkboxes/radio buttons
for the various academies that aim to filter cards by academy.